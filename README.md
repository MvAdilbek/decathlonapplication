## DECATHLON

Your task is to write a Java program that would calculate the results of a Decathlon competition.
INPUT
A CSV file containing the raw results of the competition (see ‘results.csv’ file in attachment).
OUTPUT
An XML file containing all the athletes in ascending order of their places. Athletes should have
all the result data from the CSV file plus the calculated total score and the place in the
competition.
In case of equal scores, athletes must share the places, e.g. 3-4 and 3-4 instead of 3 and 4.
The rules for the point calculation can be found here: ​http://en.wikipedia.org/wiki/Decathlon
TECHNICAL REQUIREMENTS

- JDK 11 should be used
- The app should not use any external libraries in addition to the Java standard API
- The tests can use external libraries (Junit, Mockito, etc)
- Your project should be build using Maven
WHAT WE WOULD LIKE TO SEE
- Code design should be simple yet flexible allowing easy modifications (for example,
imagine some other input/output formats)
- Java is an object-oriented programming language – use its full power!
- Clean code principles must be followed
- Good code coverage with (at least) unit-tests
DELIVERY
Please post your app to your Github (or any other online repo) and send us the link. We’ll be
running both your app and all tests.
There is no strict time limit, but try to finish the assignment in a reasonable amount of time.
Please do not hesitate to contact us, should you have any questions
Good luck!

