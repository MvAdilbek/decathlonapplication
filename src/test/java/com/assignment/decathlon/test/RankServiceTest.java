package com.assignment.decathlon.test;

import com.assignment.decathlon.service.impl.RankService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;

@RunWith(Parameterized.class)
public class RankServiceTest extends Assert {

    @Parameterized.Parameter()
    public RankTestModel rankTestModel;

    @Parameterized.Parameters
    public static Collection<RankTestModel> data() {
        return Arrays.asList(
                new RankTestModel(Arrays.asList("1", "2", "3"), Arrays.asList(100, 150, 200)),
                new RankTestModel(Arrays.asList("1", "2-3", "2-3", "4"), Arrays.asList(100, 150, 150, 200)),
                new RankTestModel(Arrays.asList("1-2", "1-2"), Arrays.asList(100, 100))
        );
    }

    @Test
    public void test_calculate_points() {
        RankService rankService = new RankService();
        Map<Integer, String> ranksResult = rankService.conferRanks(rankTestModel.points);

        for (int i = 0; i < rankTestModel.points.size(); i++) {
            String expectedRank = rankTestModel.ranks.get(i);
            String rank = ranksResult.get(rankTestModel.points.get(i));
            assertEquals(expectedRank, rank);
        }
    }

    private static class RankTestModel {
        List<String> ranks;
        List<Integer> points;

        public RankTestModel(List<String> ranks, List<Integer> points) {
            this.ranks = ranks;
            this.points = points;
        }
    }
}
