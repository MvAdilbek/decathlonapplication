package com.assignment.decathlon.test;

import com.assignment.decathlon.service.impl.CalculatePointsService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

@RunWith(Parameterized.class)
public class CalculatePointsServiceTest extends Assert {

    @Parameterized.Parameter(0)
    public Float performance;
    @Parameterized.Parameter(1)
    public Integer expectedPoints;

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        Object[][] data = new Object[][]{{10.395f, 1000}, {10.827f, 900}, {11.278f, 800}, {11.756f, 700}};
        return Arrays.asList(data);
    }

    @Test
    public void test_calculate_points() {
        CalculatePointsService calculatePointsService = new CalculatePointsService();
        Integer resultPoints = calculatePointsService.calculatePoints(performance);
        assertEquals(expectedPoints, resultPoints);
    }
}
