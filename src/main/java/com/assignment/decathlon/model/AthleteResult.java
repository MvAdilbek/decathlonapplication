package com.assignment.decathlon.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class AthleteResult {
    private String name;
    private List<Float> performanceList;
    private Integer totalPoints;
    private String rank;

    public AthleteResult() {
    }

    public AthleteResult(String name, List<Float> performanceList, Integer totalPoints, String rank) {
        this.name = name;
        this.performanceList = performanceList;
        this.totalPoints = totalPoints;
        this.rank = rank;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Float> getPerformanceList() {
        if (performanceList == null) {
            performanceList = new ArrayList<>();
        }
        return performanceList;
    }

    public Integer getTotalPoints() {
        return totalPoints;
    }

    public void setTotalPoints(Integer totalPoints) {
        this.totalPoints = totalPoints;
    }

    public String getRank() {
        return rank;
    }

    public void setRank(String rank) {
        this.rank = rank;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AthleteResult that = (AthleteResult) o;
        return Objects.equals(name, that.name) &&
                Objects.equals(performanceList, that.performanceList) &&
                Objects.equals(totalPoints, that.totalPoints) &&
                Objects.equals(rank, that.rank);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, performanceList, totalPoints, rank);
    }
}
