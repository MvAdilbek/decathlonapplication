package com.assignment.decathlon;

import com.assignment.decathlon.coder.decoder.AthleteResultCsvDecoder;
import com.assignment.decathlon.coder.encoder.AthleteResultXmlEncoder;
import com.assignment.decathlon.model.AthleteResult;
import com.assignment.decathlon.service.ICalculatePointsService;
import com.assignment.decathlon.service.impl.CalculatePointsService;
import com.assignment.decathlon.service.impl.RankService;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class DecathlonApplication {
    public static void main(String[] args) {
        String csvFilePath = "src/main/resources/results.csv";
        String xmlFilePath = "src/main/resources/results.xml";

        AthleteResultCsvDecoder athleteResultCsvDecoder = new AthleteResultCsvDecoder();
        List<AthleteResult> athleteResultList = athleteResultCsvDecoder.decodeFile(csvFilePath);

        setTotalPoints(athleteResultList);
        setRanks(athleteResultList);
        athleteResultList.sort(Comparator.comparing(AthleteResult::getTotalPoints).reversed());

        AthleteResultXmlEncoder athleteResultXmlEncoder = new AthleteResultXmlEncoder();
        athleteResultXmlEncoder.encode(xmlFilePath, athleteResultList);
    }

    private static void setTotalPoints(List<AthleteResult> athleteResults) {
        ICalculatePointsService calculatePointsService = new CalculatePointsService();
        for (AthleteResult athleteResult : athleteResults) {
            List<Float> performanceList = athleteResult.getPerformanceList();
            Integer totalPoints = calculatePointsService.calculateTotalPoints(performanceList);
            athleteResult.setTotalPoints(totalPoints);
        }
    }

    private static void setRanks(List<AthleteResult> athleteResultList) {
        List<Integer> totalPointsList =
                athleteResultList.stream().map(AthleteResult::getTotalPoints).collect(Collectors.toList());
        RankService rankService = new RankService();
        Map<Integer, String> rankList = rankService.conferRanks(totalPointsList);
        for (AthleteResult athleteResult : athleteResultList) {
            athleteResult.setRank(rankList.get(athleteResult.getTotalPoints()));
        }
    }
}
