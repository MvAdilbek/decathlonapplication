package com.assignment.decathlon.service;

import java.util.List;
import java.util.Map;

public interface IRankService {
    Map<Integer, String> conferRanks(List<Integer> pointList);
}
