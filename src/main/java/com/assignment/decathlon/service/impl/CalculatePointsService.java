package com.assignment.decathlon.service.impl;

import com.assignment.decathlon.service.ICalculatePointsService;

import java.util.List;

public class CalculatePointsService implements ICalculatePointsService {

    /**
     * @param p (performance)
     * @return points
     * @apiNote Points = INT(A(B — P)C) for track events (faster time produces a higher score)
     * Event	A	    B	C
     * 100 m	25.4347	18	1.81
     */
    @Override
    public Integer calculatePoints(Float p) {
        float a = 25.4347f;
        float b = 18f;
        float c = 1.81f;
        return (int) (Math.pow(b - p, c) * a);
    }

    @Override
    public Integer calculateTotalPoints(List<Float> pList) {
        int totalPoints = 0;
        for (Float p : pList)
            totalPoints += calculatePoints(p);
        return totalPoints;
    }
}
