package com.assignment.decathlon.service.impl;

import com.assignment.decathlon.service.IRankService;

import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class RankService implements IRankService {

    @Override
    public Map<Integer, String> conferRanks(List<Integer> pointList) {
        pointList.sort(Comparator.reverseOrder());
        Map<Integer, Long> rankCollect = pointList.stream()
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));

        Map<Integer, String> rankList = new HashMap<>(rankCollect.size());

        int rank1 = 1;
        int rank2 = 1;
        int lastPoint = Integer.MIN_VALUE;

        for (Integer point : pointList) {
            if (point != lastPoint && rank2 > rank1)
                rank1 = rank2 + 1;

            if (rankCollect.get(point) != 1L) {
                rank2 = (int) (rank1 + rankCollect.get(point) - 1);
                rankList.put(point, rank1 + "-" + rank2);
            }
            else {
                rankList.put(point, String.valueOf(rank1++));
            }

            lastPoint = point;
        }
        return rankList;
    }
}
