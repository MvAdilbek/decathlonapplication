package com.assignment.decathlon.service;

import java.util.List;

public interface ICalculatePointsService {
    Integer calculatePoints(Float p);

    Integer calculateTotalPoints(List<Float> pList);
}
