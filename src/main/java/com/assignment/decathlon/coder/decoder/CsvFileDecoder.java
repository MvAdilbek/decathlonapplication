package com.assignment.decathlon.coder.decoder;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public abstract class CsvFileDecoder<T> implements FileDecoder<T> {
    private static String COMMA_DELIMITER = ";";

    public static String getCommaDelimiter() {
        return COMMA_DELIMITER;
    }

    public static void setCommaDelimiter(String commaDelimiter) {
        COMMA_DELIMITER = commaDelimiter;
    }

    @Override
    public List<T> decodeFile(String filepath) {
        return this.decodeFile(new File(filepath));
    }

    @Override
    public List<T> decodeFile(File file) {
        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            return decodeFile(br);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<T> decodeFile(Reader reader) throws IOException {
        return this.decodeFromCsv(new BufferedReader(reader));
    }

    public List<T> decodeFromCsv(BufferedReader br) throws IOException {
        List<T> itemList = new ArrayList<>();

        String line;
        while ((line = br.readLine()) != null && !line.isBlank()) {
            String[] values = line.split(COMMA_DELIMITER);
            T item = convertFromCsvValues(values);
            itemList.add(item);
        }

        return itemList;
    }

    protected abstract T convertFromCsvValues(String[] values);
}
