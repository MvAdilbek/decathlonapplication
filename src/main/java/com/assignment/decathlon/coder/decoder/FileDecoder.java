package com.assignment.decathlon.coder.decoder;

import java.io.File;
import java.io.IOException;
import java.io.Reader;
import java.util.List;

public interface FileDecoder<T> {
    List<T> decodeFile(String filepath);

    List<T> decodeFile(File file);

    List<T> decodeFile(Reader reader) throws IOException;
}
