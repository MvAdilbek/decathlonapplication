package com.assignment.decathlon.coder.decoder;

import com.assignment.decathlon.model.AthleteResult;

public class AthleteResultCsvDecoder extends CsvFileDecoder<AthleteResult> {

    @Override
    protected AthleteResult convertFromCsvValues(String[] values) {
        AthleteResult athleteResult = new AthleteResult();
        athleteResult.setName(values[0]);
        for (int i = 1; i < values.length; i++) {
            Float point = Float.valueOf(values[i]);
            athleteResult.getPerformanceList().add(point);
        }
        return athleteResult;
    }
}
