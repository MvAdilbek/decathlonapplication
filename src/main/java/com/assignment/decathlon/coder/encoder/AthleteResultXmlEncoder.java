package com.assignment.decathlon.coder.encoder;

import com.assignment.decathlon.model.AthleteResult;

import java.util.List;

public class AthleteResultXmlEncoder extends XmlFileEncoder<AthleteResult> {

    @Override
    protected String objectListToXml(List<AthleteResult> athleteResultList) {
        StringBuilder xmlStringBuilder = new StringBuilder(41 + athleteResultList.size() * 200);

        xmlStringBuilder.append("<athleteResultList>\n");

        for (AthleteResult athleteResult : athleteResultList)
            addXmlItem(xmlStringBuilder, athleteResult);

        xmlStringBuilder.append("</athleteResultList>");

        return xmlStringBuilder.toString();
    }

    private void addXmlItem(StringBuilder sb, AthleteResult athleteResult) {
        sb
                .append("   <athleteResult>\n")
                .append("      <name>").append(athleteResult.getName()).append("</name>\n")
                .append("      <performances>").append(athleteResult.getPerformanceList()).append("</performances>\n")
                .append("      <totalPoints>").append(athleteResult.getTotalPoints()).append("</totalPoints>\n")
                .append("      <rank>").append(athleteResult.getRank()).append("</rank>\n")
                .append("   </athleteResult>\n");
    }
}
