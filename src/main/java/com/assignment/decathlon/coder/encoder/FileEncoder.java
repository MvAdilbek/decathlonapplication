package com.assignment.decathlon.coder.encoder;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

public interface FileEncoder<T> {
    void encode(String filepath, List<T> tList);

    void encode(File file, List<T> tList);

    void encode(FileWriter writer, List<T> tList) throws IOException;
}
