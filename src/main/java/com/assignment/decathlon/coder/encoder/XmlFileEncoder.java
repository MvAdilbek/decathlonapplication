package com.assignment.decathlon.coder.encoder;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

public abstract class XmlFileEncoder<T> implements FileEncoder<T> {

    @Override
    public void encode(String filepath, List<T> ts) {
        this.encode(new File(filepath), ts);
    }

    @Override
    public void encode(File file, List<T> ts) {
        try (FileWriter fileWriter = new FileWriter(file)) {
            encode(fileWriter, ts);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void encode(FileWriter writer, List<T> ts) throws IOException {
        String xmlString = objectListToXml(ts);
        writer.write(xmlString);
    }

    protected abstract String objectListToXml(List<T> ts);
}
